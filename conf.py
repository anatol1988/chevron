# -*- coding: utf-8 -*-
"""
Configuration for chevron script
"""

#: Debug flag. If True - some debug information will be printed
DEBUG = True

#: Whitelist for trigger
WHITELIST = ['Sandbag',
             'Roose',
             'Voyager',
             'Chimpy']

#: Trigger string
TRIGGER = '@chevron'

#: Chevron parameters
CHEVRONS = {'0': '50 50 50',
            '1': '20 20 20',
            '2': '30 30 30',
            '3': '30 30 30',
            '4': '30 30 30',
            '5': '30 30 30',
            '6': '30 30 30'}

#: Command running from shell
COMMAND = './bash_script.sh'

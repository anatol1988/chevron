#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
    chevron
    ~~~~~~~

    :author: Anatol Karalkou
    :contact: anatol1988@gmail.com
"""

import argparse
import re
import shelve
import datetime
import subprocess

import conf


def print_debug(debug_str):
    """
    Prints string if debug flag is set
    """

    if conf.DEBUG:
        print(debug_str)


DATE_TIME_PATTERN = re.compile(r'\[(.*?)\]')
NAME_PATTERN = re.compile(r':\s(\w.*?):')
INDEX_PATTERN = re.compile(conf.TRIGGER + r'\s(.\d?)')


def find_white_name(line):
    """
    Finds white name in list.
    Returns name if finded, else - None

    Parameters
    __________
    line : log line
    """

    name = None

    for list_name in conf.WHITELIST:
        if ' ' + list_name +':' in line:
            name = list_name
            print_debug('White name is finded: ' + name)

    return name


def parse_line(line):
    """
    Parses text line for time, name and index

    Parameters
    __________
    line : log line
    """

    time_string = DATE_TIME_PATTERN.search(line).group(1)
    date_time = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M:%S')
    index = INDEX_PATTERN.search(line).group(1)
    print_debug('Line parsed: time={0}, index={1}'.format(date_time,
                                                          index))
    return {'date_time': date_time, 'index': index}


def call_command(name, index):
    """
    Calls predefined command with parameters

    Parameters
    __________
    name : user name
    index : shevron index
    """

    cmd_str = '{0} "{1}" "{2}"'.format(conf.COMMAND, name, conf.CHEVRONS[index])
    print_debug('Calling command: ' + cmd_str)
    subprocess.call(cmd_str, shell=True)


def chevron(file_name):
    """
    Process log file

    Parameters
    __________
    log_file : logfile name
    """

    with open(file_name, 'r') as log:
        time_shelve = shelve.open('timedate')
        prev_time = (time_shelve['last_time']
                     if ('last_time' in time_shelve)
                     else datetime.datetime(datetime.MINYEAR, 1, 1, 0, 0, 0))
        last_time = prev_time
        print_debug('Last time: ' + str(last_time))
        lines = log.readlines()

        for line in lines:
            if conf.TRIGGER in line:
                name = find_white_name(line)

                if name:
                    parsed = parse_line(line)

                    if parsed['date_time'] > prev_time:
                        call_command(name, parsed['index'])
                        last_time = parsed['date_time']

        print_debug('Last time: ' + str(last_time))
        time_shelve['last_time'] = last_time
        time_shelve.close()


def main():
    """
    Processing input parameters and calling chevron()
    """

    parser = argparse.ArgumentParser(
        description='Process log file')
    parser.add_argument('input', help='logfile name')
    args = parser.parse_args()
    chevron(args.input)


if __name__ == '__main__':
    main()
